import React, { Component } from 'react';
import HeroSection from '../components/HeroSection/HeroSection';
import HowItWorks from '../components/HowItWorks/HowItWorks';
import SaveStore from '../components/SaveStore/SaveStore';
import TimeQuote from '../components/TimeQuote/TimeQuote';
import PressQuotes from '../components/PressQuotes/PressQuotes';
import Footer from '../components/Footer/Footer';

class Home extends Component {
  login() {
    this.props.auth.login();
  }
  render() {
    console.log('auth cre', this.props.auth.isAuthenticated);
    const { isAuthenticated } = this.props.auth;
    return (
      <div className="container-fluid mainWrapper">
        {
          isAuthenticated() && (
              <div>
                <h2>Welcome</h2>
              </div>
            )
        }
        {
          !isAuthenticated() && (
            <div>
              <HeroSection />
              <HowItWorks />
              <SaveStore />
              <TimeQuote />
              <PressQuotes />
              <Footer />
            </div>
            )
        }
      </div>
    );
  }
}

export default Home;
