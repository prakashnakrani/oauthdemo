import React from 'react';
const Header = (props) => { 
    return (
        <div className="header-0-4">
            <div className="headerWrap-0-5">
                <div className="rightContent-0-6">
                    <h1 className="h1 headerText-0-7">
                        If there's a better price, we'll find it.
                    </h1>
                    <h2 className="headerBottomText-0-8">
                        Stop wasting money – Honey finds you the Internet's best discount codes.
                    </h2>
                </div>
            </div>
        </div>
    )
}

export default Header;