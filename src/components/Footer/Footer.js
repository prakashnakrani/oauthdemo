import React from 'react';

const Footer = (props) => {
    return (
        <div className="main-0-52">
            <div className="topBorder-0-53"></div>
            <div className="links-0-56">
                <div className="main-0-58">
                    <a className="link-0-59" href="https://help.joinhoney.com">Help</a>
                    <a className="link-0-59" href="/careers">Careers</a>
                    <a className="link-0-59" href="/press">Press</a>
                    <a className="link-0-59" href="/blog">Blog</a>
                    <a className="link-0-59" href="/privacy">Privacy</a>
                    <a className="link-0-59" href="/copyright">Copyright</a>
                    <a className="link-0-59" href="/patents">Patents</a>
                    <a className="link-0-59" href="/terms">Terms</a>
                </div>
            </div>
        </div>
    )
}

export default Footer;