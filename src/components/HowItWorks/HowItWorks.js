import React from 'react';

const HowItWorks = (props) => {
    return(
        <div className="section-0-16 howItWorks-0-17">
            <h2 className="h2 title-0-15">How it works</h2>
            <div className="howitWorksContent-0-18">
                <div className="howItWorksCol-0-19">
                    <img alt="" src="https://cdn.joinhoney.com/images/landing/honey-button/install.svg" />
                    <p className="howItWorksColTitle-0-20">
                        Install in Seconds
                    </p>
                    <p className="howItWorksColDescription-0-21">
                        Takes just two clicks and it's 100% free.
                    </p>
                </div>
                <div className="howItWorksCol-0-19">
                    <img alt="" src="https://cdn.joinhoney.com/images/landing/honey-button/loading-laptop.svg" />
                    <p className="howItWorksColTitle-0-20">Shop like Normal</p>
                    <p className="howItWorksColDescription-0-21">
                        We'll find every working promo code online.
                    </p>
                </div>
                <div className="howItWorksCol-0-19">
                    <img alt="" src="https://cdn.joinhoney.com/images/landing/honey-button/hand-dollar.svg" />
                    <p className="howItWorksColTitle-0-20">Save Instantly</p>
                    <p className="howItWorksColDescription-0-21">
                        We'll apply the code with the biggest savings.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default HowItWorks;