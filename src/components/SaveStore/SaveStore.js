import React from 'react';

const SaveStore = (props) => {
    return(
        <div className="section-0-16 saveStores-0-22">
            <h2 className="h2 title-0-15">Save at thousands of stores</h2>
            <div className="saveStoresDescription-0-23">
                From clothes to pizza, pay less for the things you're already buying online.
            </div>
            <div className="saveStoresLogos-0-24">
                <a href="/shop/groupon" className="hide-0-38" target="_blank" rel="noopener noreferrer">
                    <img alt="groupon" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/groupon.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/nike" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="nike" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/nike.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/amazon" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="amazon" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/amazon.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/pizzahut" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="pizzahut" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/pizzahut.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/expedia" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="expedia" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/expedia.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/nordstrom" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="nordstrom" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/nordstrom.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/sephora" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="sephora" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/sephora.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/bloomingdales" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="bloomingdales" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/bloomingdales.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/macys" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="macys" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/macys.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/jcrew" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="jcrew" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/jcrew.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/forever21" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="forever21" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/forever21.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/marriott" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="marriott" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/marriott.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/finish-line" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="finish-line" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/finish-line.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/best-buy" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="best-buy" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/best-buy.svg" className="saveStoresLogo-0-25" />
                </a>
                <a href="/shop/crate-and-barrel" className="false" target="_blank" rel="noopener noreferrer">
                    <img alt="crate-and-barrel" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/crate-and-barrel.svg" className="saveStoresLogo-0-25" />
                </a>
            </div>
        </div>
    )
}

export default SaveStore;