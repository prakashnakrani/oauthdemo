import React from 'react';

const TimeQuote = (props) => {
    return(
        <div className="section-0-16 timeQuote-0-26" >
            <img alt="Honey Extension" className="honeyExtension-0-27 lazyautosizes lazyloaded" data-sizes="auto" data-src="https://cdn.joinhoney.com/images/landing/honey-button/honey-extension.png" src="https://cdn.joinhoney.com/images/landing/honey-button/honey-extension.png" />
            <div >
                <img alt="Time Quote" className="timeQuoteImage-0-28 lazyautosizes lazyloaded" data-sizes="auto" data-src="https://cdn.joinhoney.com/images/landing/honey-button/time-quote.png" src="https://cdn.joinhoney.com/images/landing/honey-button/time-quote.png" />
            </div>
        </div>
    )
}

export default TimeQuote;