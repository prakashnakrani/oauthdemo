import React from 'react';

const PressQuotes = (props) => {
    return(
        <div className="section-0-16 pressQuotes-0-30">
            <div className="pressQuotesInner-0-31">
                <div className="pressQuote-0-29">
                    <img alt="Honey Extension" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/consumer-reports.svg" className="pressQuoteLogo-0-33" />
                    <div className="pressQuoteDescription-0-34">
                        “Forget about Googling coupon codes.”
                    </div>
                </div>
                <div className="pressQuote-0-29">
                    <img alt="Honey Extension" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/bustle.svg" className="pressQuoteLogo-0-33" />
                    <div className="pressQuoteDescription-0-34 bustle-0-35">
                        “It’s kind of a no-brainer.”
                    </div>
                </div>
                <div className="pressQuote-0-29">
                    <img alt="Honey Extension" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/cnet.svg" className="pressQuoteLogo-0-33" />
                    <div className="pressQuoteDescription-0-34">
                        “Use Honey to save money on Amazon purchases.”
                    </div>
                </div>
                <div className="pressQuote-0-29">
                    <img alt="Honey Extension" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/google.svg" className="pressQuoteLogo-0-33" />
                    <div className="pressQuoteDescription-0-34 google-0-36">
                        Chrome Store Web Editor's Pick
                    </div>
                </div>
                <div className="pressQuote-0-29">
                    <img alt="Honey Extension" src="https://cdn.joinhoney.com/images/landing/honey-button/store-logo/business-insider.svg" className="pressQuoteLogo-0-33" />
                    <div className="pressQuoteDescription-0-34 business-insider-0-37">
                        “Become a coupon pro with Honey.”
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PressQuotes;